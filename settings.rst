Settings
========

Here you can see all technical settings for Recommend service setup on the site.

1. General
-----------

In General Settings you can add the general information for the site. Also here you can change the status to Preview if you first want recommendations not to be shown to all users on the site. This is mostly used when Recommend is newly installed and you want to check its work before making it active. Also you can use preview links here to enable or disable preview mode for each user individually.

.. image:: images/settings_general.png
  :width: 75 %
  :align: center

2. Product Attributes
----------------------

Here you can add and configure different product attributes which can then be applied in panel rules to sort recommendations by the needed criteria, e.g. narrowing the selection only to products with specific attributes, or excluding products with specific attributes from recommendations. 

**To add a new product attribute you should:**

-Click on the red Add New Product Attribute button.

-Enter the Title (this will be the name of the attribute).

-Enter Attribute Code (this will be the system name of the product attribute).

-And enter Attribute Type (the type of the data for this attribute sent from Magento). 

-Then Save. 

**To edit a product attribute simply click on it and the page to edit will open.**


.. image:: images/settings_product_attributes_1.png
  :width: 75 %
  :align: center

|

.. image:: images/settings_product_attributes_2.png
  :width: 75 %
  :align: center

3. Customer Attributes
-----------------------

Here you can add and configure customer attributes such as gender, in order to apply them in panel rules and hereby create segmentations options to sort recommendations with. Remember that customer attributes will work only when your Magento sends out the data as attributes to Recommend. 

**To add a new customer attribute you should:**

-Click on the red Add New Product Attribute button.

-Enter the Title (this will be the name of the attribute).

-Enter the Code (this will be the system name of the product attribute).

-Enter Attribute Type (the type of the data for this attribute sent from Magento). 

-Then Save. 

**To edit a customer attribute simply click on it and the page to edit will open.**


.. image:: images/settings_customer_attributes.png
  :width: 75 %
  :align: center

4. Users
---------

Here you can see the list of the users that have access to Recommend Control Panel. 

**To add a new user you should:**

-Click on the red Invite User button.

-Enter user’s email.

-Choose Full or Restricted Access option.

-Click Invite.


.. image:: images/settings_users.png
  :width: 75 %
  :align: center

Integration/API Keys
--------------------

Here you can integrate Recommend into your Magento store.

For this it’s best to use Recommend module which can be downloaded
from `Magento Marketplace <https://marketplace.magento.com/superb-module-recommend.html/>`_.

Also in this section you will see the list of  your Recommend API Keys for Magento.
API Keys serve to manually integrate Recommend into your Magento.
You can add an API Key clicking on the red Add New API Key button and generating it.