Audience
========

Contains reports for the customers data.

Page Views
----------

Shows reports for the number of general page views for selected time period,
as well as specially for returning customers, guest, new and registered customers.

.. image:: images/reports_audience_pageviews.png
  :width: 75 %
  :align: center

Visits
------

Shows reports for the general number of panel visits for selected time period,
as well as specially for returning customers, guest, new and registered customers.

.. image:: images/reports_audience_visits.png
  :width: 75 %
  :align: center

|