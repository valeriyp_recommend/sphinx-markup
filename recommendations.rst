Recommendations
===============

In Recommendations section you can customize everything for your site recommendations. 
Choose here which pages panels should display on and adjust each panel settings. You will need to select the necessary parameters for product recommendations on every panel. 
If you want to send marketing communication to customers use «Win-back Emails» option and configure emails there.

Panels
------
Here you will see the list of all existing panels you can apply on your site.

|

**How to add a new panel:**

1.Click on the red Add New Panel button above.

2.Then enter: Panel ID (short description of the panel function), Title (the name which this panel will show on the site), change Status to Active to turn the panel on.

3.In Panel Type select the needed option from the dropdown menu depending on the principle you would like products recommendations to be sorted by. The options are:

* **Recently Viewed Products** (shows those products on the panel that were recently viewed),
* **Most Popular Products** (shows best selling products on the panel),
* **Product Related Products** (shows those products on the panel which have the most appropriate relations to a specific product),
* **Recently Viewed Top Products** (shows best selling products that were recently viewed),
* **Shopping Cart related Products** (shows products that have the most appropriate relations to products in the shopping cart),
* **Search Term Related Products** (shows products containing the search term in their name),
* **Random Products** (shows random products without any specific selections and mostly used either for test purposes or when there are not enough products on the store to setup appropriate recommendations),
* **Search Page Suggestions** (shows products containing the search term in their name),
* **Great Deals** (shows products with discounts or great deals tags),
* **Product Related Products with Rule** (creates more specific selection for the products on the panel, the rules are applied depending on the results you need to display),
* **Most Popular Products with Rule** (creates more specific selection for best selling products on the panel).

4.When Panel Type is selected choose the minimum and maximum number of products that will be displayed on the panel. 

5.In Show Other Panel When Empty option you can set an extra panel which will be displayed instead of the current one in case when there is not enough data to build the selection for the current panel.
 
6.Remember to Save panel settings when ready. To edit an existing panel just click on its name in the list and the content layout to edit will open.

.. image:: images/recommendation_panels_1.png
  :width: 75 %
  :align: center

|

.. image:: images/recommendation_panels_2.png
  :width: 75 %
  :align: center

Panel Placement
---------------

This is the section where you can configure which panels will be placed on your site. Basically, you can select which panels from existing panels list you added in Panels section will be displayed on each particular website page.

Templates
---------

In this section you should add different templates containing the code to insert into Magento. These templates can then be inserted into panels.

Customer Segments
-----------------

In Customer Segments you can divide all customers to groups using different segment parameters, depending on the data you need to divide the users to. For example, you can create segments by the region customers are from, by device type, last order date, last transaction etc. 

1.Click on the red Add New Segment button above.

2.Then enter a Title for the segment, and choose the necessary criteria. Clicking on Add Statement button you can choose the needed parameter and the rules applied to it. 

3.Then Save. 

**To edit a Segment simply click on it and then Save after the changes.**

.. image:: images/recommendations_customer_segments_1.png
  :width: 75 %
  :align: center

|

.. image:: images/recommendations_customer_segments_2.png
  :width: 75 %
  :align: center

Win-back Emails
---------------

These are triggered emails (email marketing communication) which will help you bring more customers back to your site and enhance the marketing performance. Mostly triggered emails are sent to customers with abandoned carts, but you can also set them to send order reminders or take contact with a customer after some time they haven’t been visiting the site. 

|

**Here is how you can add a new email template:**

1.Click on «Add New Email Template» red button above.

2.Enter a Subject (the email subject that recipients will see), then sender’s email address in the From line (the recipients will see this address when emails come in). -In Recipient Email option choose whether all users or selected recipients should receive an email. 

3.Bcc option is used if you need to add the address of the recipient you would like to send copies of all out-coming emails.

|

Note: In Parent Win-back email you can add the main email these emails will be connected with (such option is used when you need to create a mail chain and define the main (parent) and subsiding emails for it).

4.In Send After Action option choose the time interval when emails should be sent. In Time Interval and Send Again After Interval options you can set the time period after which the first and then the recurring email will be sent.

5.In Send Again Email option you can either activate or deactivate recurring emails (setting it to True or False). 

6.Select the type of email template(Abandoned cart, Order Reminder or We Miss You in case of re-connecting with a customer). 

7.Choose the Status.

8.Save. 

.. image:: images/recommendations_winback_emails_1.png
  :width: 75 %
  :align: center

|

.. image:: images/recommendations_winback_emails_2.png
  :width: 75 %
  :align: center

|

