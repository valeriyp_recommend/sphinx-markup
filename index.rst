.. Recommend documentation master file, created by
   sphinx-quickstart on Mon Feb 19 16:40:09 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Recommend's documentation!
=====================================

.. toctree::
   :maxdepth: 3

   dashboard
   recommendations
   messaging
   reports
   settings
