Messaging
=========
In Messaging section you can set up email smart campaigns, push notifications and mass campaigns. Here you can also add new and edit all templates which will be used in all campaigns.

.. toctree::
  :maxdepth: 2

  Smart Campaigns
  Mass Campaigns
  Templates


