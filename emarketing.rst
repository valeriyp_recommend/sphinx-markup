Email Marketing
===============

Contains all email related reports.

Email Performance
-----------------

Shows general data for email performance including the general number of sent emails
for selected time period, the email click and open rate, and the revenue emails brought.

.. image:: images/email_marketing_email_performance.png
  :width: 75 %
  :align: center

Abandoned Carts
---------------

Shows reports for the rate of abandoned shopping carts, the number
of abandonment emails sent and the revenue gained from these emails.

.. image:: images/email_marketing_abandoned_carts.png
  :width: 75 %
  :align: center

Sent Mail
---------

Shows all sent emails for each user for selected time period.
You can click on an email to open it and see how it looks for the user.

.. image:: images/email_marketing_sent_email.png
  :width: 75 %
  :align: center

|