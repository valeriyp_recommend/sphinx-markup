Templates
=========

Here you can add and edit your smart campaigns emails. 

**Here is how to add a new template:**

-Click on Add New Email Template red button above.

-Enter the name of the campaign in Title line.

-Enter the Subject (the email subject that recipients will see).

-Add sender’s email address in the From line (the recipients will see this address when emails come in).

-Select campaign type (Email or Push Notifications).

-Add the template markup into the section below. 

|

Note: You can see how email will look clicking on Preview button.

-Remember to save your template. 