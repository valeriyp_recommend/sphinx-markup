Ecommerce
=========

Contains commercial reports for the data tracked from the site.

Panel Performance
-----------------

Shows reports for the revenue and click rate received from Recommend panels.

Overview
--------

Shows reports for the general and assisted revenue.

.. image:: images/reports_ecommerce_overview.png
  :width: 75 %
  :align: center

Sales Performance
-----------------

Shows reports with the data for number of orders, quantity and assisted revenue.

.. image:: images/reports_ecommerce_sales_performance.png
  :width: 75 %
  :align: center

Product Performance
-------------------

Shows reports for the number of product views, their conversion rate, unique purchases and revenue.

.. image:: images/reports_ecommerce_product_performance.png
  :width: 75 %
  :align: center

Category Performance
--------------------

Shows reports for different categories performance including category views, conversion rate, unique purchases and revenue.

.. image:: images/reports_ecommerce_category_performance.png
  :width: 75 %
  :align: center

|