Dashboard
=========

Dashboard displays all current tracking information of the website. It shows all currently active customers on the site (including details of users’ location, type of device and number of registered customers as well as guest visitors).

.. image:: images/dashboard_2.png
  :width: 75 %
  :align: center

|

.. image:: images/dashboard_1.png
  :width: 75 %
  :align: center

|
