Smart Campaigns
===============

Here you can add and edit your customer email smart campaigns. 
To add a new email smart campaign click on «Add New Smart Campaigns» button. Or click on campaign name in the list if you want to edit an existing campaign.

How to set up a new campaign:
-----------------------------

**1.You need to begin with the «Start» node.**


-Click on the gear sign at the top left corner. 

-Press «Add Rule» button in the window that opens.

-In «Events» rule you can choose the event after which campaign will start (e.g. Customer has viewed the page).
 
-In Order rule you can select if customer has any orders. 

-In Cart rule you can select anything related to customers’ shopping cart. 

-In Customer rule you can select attributes related to customers’ data (e.g. if customer is subscribed, date of last visit etc.). 

-When rules are set click Save and then Apply. 

**You can edit and add more rules any time clicking on the Gear sign.**
**Remember to connect the Start node with the next node by clicking and dragging the green circle from this node to the next one.**



.. image:: images/Messaging_Smart_Campaigns_Pic1.png
  :width: 75 %
  :align: center

|

**2. «Delay» node.This node is used to set the time delay after which next actions will occur.**

-Click on the Gear sign.

-Set here some specific time to wait before next campaign action or determine the date/time when further action occurs. 

**Remember to connect the Delay node with the next node by clicking and dragging the green circle from this node to the next one.**

.. image:: images/Messaging_Smart_Campaigns_Pic2.png
  :width: 75 %
  :align: center

|

**3. «Condition» node.**

-Click on the Gear sign.

-Click on Add Rule button.

-Set the rule(s) for this condition. 

-In Order, Cart and Customer sections you can again select the necessary rules for this node.

.. image:: images/Messaging_Smart_Campaigns_Pic3.png
  :width: 75 %
  :align: center

.. image:: images/Messaging_Smart_Campaigns_Pic4.png
  :width: 75 %
  :align: center

**Note that Condition node can be connected with the next node in two possible ways - by dragging the green «Next» circle to the following node, or when necessary by additionally dragging another red «Break» circle to an additional node (this way you can set the time interval or date when the current condition should cease its function).**

|

**4. «Event» node.**

In this node you can set a customer action that would serve as a point on which further or previous campaign actions would depend (e.g. «Customer has placed an order» action or «Customer has opened the campaign»). See pic.5.



.. image:: images/Messaging_Smart_Campaigns_Pic5.png
  :width: 75 %
  :align: center

|

**5. «Send» node.**

In this node you can select an email template which will be used for emails sent at this campaign point. You can add and use as many templates in campaign as needed. To add or edit the existing template simply click on «Add new» or «Edit» button directly from the Send node, or you can also configure templates from the «Messaging» > »Templates» section in control panel. 

|

**6. «Fork» node.**

You can use this node when you need to create two different child processes for the current campaign flow. You can set the necessary rules in this node and select whether all conditions should be true or at least one one of them. Then use the «Yes» and «No» connectors to fork. 

|

Note: A good example of using the Fork node is when you need a customer to leave or continue the campaign at some point depending on this point conditions.

.. image:: images/Messaging_Smart_Campaigns_Pic6.png
  :width: 75 %
  :align: center

|

**7. «Annotations» node.**

In this node you can add the notes and add it to any point of the campaign when needed. 

|

**8. «End» node.**

The end node is used to mark the end of the campaign. Also you should close one of the processes in the «Fork» node with the «End» if customer has to leave the campaign at that point conditions. (see Pic.7).

.. image:: images/Messaging_Smart_Campaigns_Pic7.png
  :width: 75 %
  :align: center


----------------


**9.As soon as your campaign is ready you click on the Save button**

(this will save the campaign in your drafts). Or click on «Publish» button to launch it.

|

Note: There is also a Gear sign on the left from the Save button, clicking on it you can add testers’ email addresses to test a campaign.

|

**10.When your campaign is published you can change the status any time clicking on Active, Testing and Paused buttons at the top right corner.** You can also edit testers’ email from here clicking on the red Options button next to the status buttons. When you need to check the progress on the current campaign click on the View Stats button above the Options button and it will show the statistics of the current campaign performance. 

---------------------

**Examples**


Below are some examples of most common email campaign types with the working strategies used by our clients: 

.. image:: images/Order_Reminder.png
  :width: 75 %
  :align: center

.. image:: images/We_Miss_You.png
  :width: 75 %
  :align: center

.. image:: images/Abandoned_Cart.png
  :width: 75 %
  :align: center